# -*- coding: utf-8 -*-
import re
import os
import configparser
from qgis.core import QgsProject, QgsVectorLayer, QgsRasterLayer
import codecs

def get_cfg():
    """ Returns the configuration file """
    plugin_dir = os.path.split(os.path.dirname(__file__).replace("\\", "/"))[0]
    defaultcfg = "{}/locator/settings.cfg".format(plugin_dir)
    cfgfile = defaultcfg
    return cfgfile


def get_setting(option, section=None):
    """ Returns requested setting from config """
    cfgfile = get_cfg()
    config = configparser.RawConfigParser()
    config.read_file(codecs.open(cfgfile, 'r', 'utf-8'))
    if section:
        try:
            return config.get(section, option)
        except configparser.NoSectionError:
            pass
    else:
        for section in config.sections():
            try:
                return config.get(section, option)
            except configparser.NoOptionError:
                pass
    if section:
        error = "{}: {}".format(section, option)
    else:
        error = "{}".format(section)
    viesti = "Setting  {} was not Found.".format(error)
    print(viesti)
    return False


def get_layer_list(re_string=None,
                   geometry_types=None,
                   raster_layers=False):
    """ Geometry types are the following:
        0: Point
        1: Line
        2: Polygon
        If you have set a geometry type then you shouldn't have set
            raster_layers=True
        because raster layers don't have geometry, they are pictures. """
    if not geometry_types:
        geometry_types = [0, 1, 2]
    all_layers = QgsProject.instance().mapLayers().values()
    layer_list = []
    for layer in all_layers:
        if (type(layer) == QgsVectorLayer
                and layer.geometryType() in geometry_types
                or
                type(layer) == QgsRasterLayer
                and raster_layers):
            if not re_string or re.search(re_string, layer.name()):
                layer_list.append(layer)
    return layer_list


def find_features(layer, field_box, search_string, full_scan=False):
    match_list = [] 
    if not full_scan:
        for feature in layer.getFeatures():
            if re.search(search_string,
                         str(feature[field_box.currentText()]),
                         re.IGNORECASE):
                match_list.append(feature)
    else:
        for feature in layer.getFeatures():
            for field_name in [field.name() for field in field_box.allItems]:
                if re.search(search_string,
                             str(feature[field_name]),
                             re.IGNORECASE):
                    match_list.append(feature)
                    break
    return match_list
