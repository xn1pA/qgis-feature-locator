# -*- coding: utf-8 -*-
import types
from PyQt5 import uic
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import *
from .locator_functions import *


def set_up_button(target_button,
                  button_name,
                  callback):
    def set_state(self, state):
        if state == 0:
            self.setIcon(self.icons[0])
            self.active = False
        else:
            self.setIcon(self.icons[1])
            self.active = True

    def toggle_state(self):
        if not self.active:
            self.setIconState(1)
        else:
            self.setIconState(0)

    # Button gui
    icon_folder_path = ':/plugins/locator/icons/'
    target_button.icons = [
        QIcon(icon_folder_path + get_setting('icon1', button_name)),
        QIcon(icon_folder_path + get_setting('icon2', button_name))]
    target_button.setText(get_setting('buttontext', button_name))
    target_button.setToolTip(get_setting('tooltip', button_name))
    # Integration of locator methods to button
    target_button.setIconState = types.MethodType(set_state, target_button)
    target_button.toggleState = types.MethodType(toggle_state, target_button)
    # Initial state
    target_button.setIconState(0)
    # Callback setup
    if callback == 'toggle':
        callback = target_button.toggleState
    if type(target_button) == QPushButton:
        target_button.pressed.connect(callback)
    elif type(target_button) == QAction:
        target_button.triggered.connect(callback)


FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'locator_dockwidget_base.ui'))
class LocatorDockWidget(QDockWidget, FORM_CLASS):
    def __init__(self, iface, parent=None):
        super(LocatorDockWidget, self).__init__(parent)
        self.setupUi(self)
        self.iface = iface
        self.canvas = iface.mapCanvas()
        # Buttons setup
        set_up_button(self.button1Button,
                      button_name='button1',
                      callback=self.button1Pressed)
        set_up_button(self.button2Button,
                      button_name='button2',
                      callback=self.button2Pressed)
        set_up_button(self.button3Button,
                      button_name='button3',
                      callback=self.button3Pressed)
        # List of buttons for toggling
        self.buttons = [self.button1Button,
                        self.button2Button,
                        self.button3Button]
        # settings buttons
        field_button = QAction()
        set_up_button(field_button,
                      button_name='button_all_fields',
                      callback='toggle')
        self.toolbar.addAction(field_button)
        self.allFieldsButton = self.toolbar.actions()[0]
        select_button = QAction()
        set_up_button(select_button,
                      button_name='button_select_all',
                      callback='toggle')
        self.toolbar.addAction(select_button)
        self.selectAllButton = self.toolbar.actions()[1]
        # Combobox setup
        self.layerBox.allItems = []
        self.updateLayerBox()
        QgsProject.instance().layersAdded.connect(self.updateLayerBox)
        QgsProject.instance().layersRemoved.connect(self.updateLayerBox)
        # Search setup
        self.searchLine.returnPressed.connect(self.searchForFeature)
        # Result table setup
        self.results = []
        self.resultsTable.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.resultsTable.itemSelectionChanged.connect(self.featureSelected)
        # Active layer changed
        self.iface.layerTreeView().currentLayerChanged.connect(
            self.activeLayerChanged)

    def updateLayerBox(self):
        """ Updates layer comboBox to match current layers """
        self.layerBox.allItems = get_layer_list()
        self.layerBox.clear()
        self.layerBox.addItems([lyr.name() for lyr in self.layerBox.allItems])
        self.layerBox.currentIndexChanged.connect(self.updateFieldBox)

    def updateIconStates(self, button_id):
        """ Toggles the button states according to active button """
        for i, button in enumerate(self.buttons):
            if i == button_id:
                button.setIconState(1)
            else:
                button.setIconState(0)

    def button1Pressed(self):
        self.updateLayerBoxIndex(
            get_layer_list(re_string=get_setting('layer', 'button1'))[0])
        self.updateFieldBoxIndex(get_setting('attribute', 'button1'))
        self.updateIconStates(0)

    def button2Pressed(self):
        self.updateLayerBoxIndex(
                get_layer_list(re_string=get_setting('layer', 'button2'))[0])
        self.updateFieldBoxIndex(get_setting('attribute', 'button2'))
        self.updateIconStates(1)

    def button3Pressed(self):
        try:
            self.updateLayerBoxIndex(self.iface.activeLayer())
        except AttributeError:
            pass
        self.updateIconStates(2)

    def updateLayerBoxIndex(self, new_layer):
        """ Updates layerbox to match input """
        for i, layer in enumerate(self.layerBox.allItems):
            if layer == new_layer:
                self.layerBox.setCurrentIndex(i)
                break

    def activeLayerChanged(self):
        if self.button3Button.active:
            self.button3Pressed()

    def updateFieldBox(self):
        """ After changing the lookup layer, update the field combobox """
        # Reset button statuses
        self.updateIconStates(-1)
        # Re-initialise the item list for fieldBox
        self.fieldBox.allItems = self.layerBox.allItems[
            self.layerBox.currentIndex()].fields()
        self.fieldBox.clear()
        self.fieldBox.addItems(
            [field.name() for field in self.fieldBox.allItems])
        self.updateResultTableGui()

    def updateFieldBoxIndex(self, new_field):
        """ Selects wanted field from field comboBox """
        for i, field in enumerate(self.fieldBox.allItems):
            if field.name() == new_field:
                self.fieldBox.setCurrentIndex(i)

    def searchForFeature(self):
        """ Update results to match the look up """
        self.results = find_features(
            self.layerBox.allItems[self.layerBox.currentIndex()],
            self.fieldBox,
            self.searchLine.text(),
            self.allFieldsButton.active
        )
        # If results are found, update the results table
        if self.results:
            self.updateResultTable()

    def updateResultTableGui(self):
        """ This method updates the GUI of results table to match
            current look up layer """
        self.resultsTable.setRowCount(0)
        fields = [field.name() for field in self.fieldBox.allItems]
        self.resultsTable.setColumnCount(len(fields))
        self.resultsTable.setHorizontalHeaderLabels(fields)

    def updateResultTable(self):
        """ This method updates the result table to match found results """
        # Clear the old results
        self.resultsTable.setRowCount(0)
        for row, feature in enumerate(self.results):
            self.resultsTable.insertRow(row)
            for column, attribute in enumerate(feature.attributes()):
                self.resultsTable.setItem(
                    row, column, QTableWidgetItem(str(attribute).strip()))
        # Bliss for eyes:
        self.resultsTable.resizeColumnsToContents()
        self.resultsTable.selectRow(0)

    def featureSelected(self):
        """ Zoom to the selected found results. Point of the whole plugin. """
        if self.results:
            if self.selectAllButton.active:
                layer = self.layerBox.allItems[self.layerBox.currentIndex()]
                layer.removeSelection()
                layer.select([feature.id() for feature in self.results])
                self.canvas.zoomToSelected(layer)
            elif self.resultsTable.currentRow() != -1:
                layer = self.layerBox.allItems[self.layerBox.currentIndex()]
                layer.removeSelection()
                feature = self.results[self.resultsTable.currentRow()]
                layer.select(feature.id())
                self.canvas.zoomToSelected(layer)
