# -*- coding: utf-8 -*-
from PyQt5.QtCore import Qt
from .resources import *
from .locator_dockwidget import LocatorDockWidget

class Locator:
    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        self.dockwidget = LocatorDockWidget(self.iface)
        self.iface.addDockWidget(Qt.LeftDockWidgetArea, self.dockwidget)
        self.dockwidget.show()

    def initGui(self):
        pass

    def unload(self):
        pass
